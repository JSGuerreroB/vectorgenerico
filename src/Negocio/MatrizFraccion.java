/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Modelo.Fraccion;
import java.util.Arrays;

/**
 *
 * @author estudiante
 */
public class MatrizFraccion {

    private SistemaFraccion[] fracciones;

    public MatrizFraccion() {
    }

    /**
     * Constructor de Matriz de tamaño rectangular o cuadrada
     *
     * @param n tamaño de filas
     * @param m tamaño de columnas
     */
    public MatrizFraccion(int n, int m) {
        validar(n);
        validar(m);
        this.fracciones = new SistemaFraccion[n];
        for (int i = 0; i < n; i++) {
            this.fracciones[i] = new SistemaFraccion(m);
        }
    }

    /**
     *
     * @param i índice de la fila
     * @param j índice de la columna
     * @param num numerador de la fracción
     * @param den denominador de la fracción
     */
    public void set(int i, int j, float num, float den) {
        this.validarFila(i);
        try {
            this.fracciones[i].set(j, num, den);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public Fraccion get(int i, int j) {
        this.validarFila(i);
        try {
            return this.fracciones[i].get(j);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void validarFila(int i) {
        if (this.fracciones == null || i < 0 || i >= this.fracciones.length) {
            throw new RuntimeException("índice de fila fuera de rango:" + i);
        }

    }

    private void validar(int i) {
        if (i < 0) {
            throw new RuntimeException("Error tamaño de matriz");
        }

    }

    @Override
    public String toString() {
        String msg = "";
        for (SistemaFraccion f : this.fracciones) {
            msg += f.toString() + "\n";
        }
        return msg;

    }

    public int getTamFilas() {
        if (this.fracciones == null) {
            throw new RuntimeException("Error matriz vacía");
        }

        return this.fracciones.length;
    }

    public int getTamColumnas() {
        if (this.fracciones == null) {
            throw new RuntimeException("Error matriz vacía");
        }
        return this.fracciones[0].getLength();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Arrays.deepHashCode(this.fracciones);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MatrizFraccion other = (MatrizFraccion) obj;
        if(this.fracciones.length==other.fracciones.length){
        for (int i = 0; i < this.fracciones.length; i++) {
            if(!this.getFila(i).equals(other.getFila(i))){
            return false;
            }
        }
        return true;
        }else{
        return false;
        }
    }

    public SistemaFraccion getFila(int fila){
        validarFila(fila);
        return this.fracciones[fila];
    } 
    
   public String getNoSeRepite() 
   {
   return ":)";
   }
   
  /**
   * Crea una matriz con fracciones randómicas
   */
   public void crearRandom()
   {
   
   
   }
    
    
}
