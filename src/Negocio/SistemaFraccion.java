/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Negocio;

import Modelo.Fraccion;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author docente
 * 
 */
public class SistemaFraccion{
    
    private Fraccion fracciones[];

    public SistemaFraccion() {
    }

    public SistemaFraccion(int n) {

        if (n <= 0) {
            throw new RuntimeException("No se pueden crear Fracciones");
        }

        this.fracciones = new Fraccion[n];
    }

    public void set(int i, float num, float den) {
        this.validar(i);
        this.fracciones[i] = new Fraccion(num, den);
    }

    private void validar(int i) {
        if (this.fracciones == null || i < 0 || i >= this.fracciones.length) {
            throw new RuntimeException("Índice fuera de rango");
        }

    }

    public Fraccion get(int i) {
        this.validar(i);
        return this.fracciones[i];
    }

    public Fraccion[] getFracciones() {
        return fracciones;
    }
    
    @Override
    public String toString() {
        String msg = "";
        for (Fraccion f : this.fracciones) {
            msg += f.toString() + "  \t";
        }
        return msg;
    }
    

    
    
    public int getLength()
    {
        return this.fracciones.length;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 73 * hash + Arrays.deepHashCode(this.fracciones);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SistemaFraccion other = (SistemaFraccion) obj;
        if(this.fracciones.length==other.fracciones.length){
        for (int i = 0; i < this.fracciones.length; i++) {
            if(!this.get(i).equals(other.get(i))){
            return false;
            }
        }
        return true;
        }else{
        return false;
        }
    }  
    
    public <T extends Comparable> void sort(T vector[])
    {
        if ( vector[0].compareTo(vector[vector.length-1])==1){
                this.switchGenerico(0, vector.length-1, vector);
        } 
        
        for(int contador1 = (int)(vector.length/1.3); contador1!=0; contador1= (int)(contador1/1.3)){
        for(int contador2 = 0; contador2+contador1!=vector.length; contador2++){
            if(vector[contador2].compareTo(vector[contador1+contador2])==1){
                this.switchGenerico(contador1+contador2, contador2, vector); 
                }
            }
        }
    }
    
    private <T extends Comparable> void switchGenerico(int n, int i, T[] vector){
            T aux = vector[i];
            vector[i]=vector[n];
            vector[n]=aux;
    }
 
    public void crearAleatorioSinRepetir(float maxNumerador, float maxDenominador)
    {
        if(this.fracciones == null){
            throw new RuntimeException("Sistema de fracciones sin tamaño");
        }
        if(maxNumerador<getLength()||maxDenominador<getLength()){
            throw new RuntimeException("Datos no validos el denominador y el numerador deben ser minimo iguales al tamaño del arreglo que es "+getLength());
        }
      
        for(int i = 0;i<this.getLength();i++){
                    Fraccion f = new Fraccion((new Random().nextFloat(maxNumerador)),(new Random().nextFloat(maxDenominador)));
                    if(!fraccionRepetida(f)){
                    this.fracciones[i]=f;
                    }else{
                    i--;
                    }
        }
    }
    
    public boolean fraccionRepetida(Fraccion f){
            boolean esta = false;
            for (int i = 0; i < this.getLength(); i++) {
                if(this.fracciones[i]!=null&&this.fracciones[i].equals(f)){
                    esta = true;
                    break;
                }
            }
        return  esta;
        }
    
}
    
    
    
    
    

