/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;
import Negocio.SistemaFraccion;
import java.util.Arrays;
import java.util.Scanner;
public class TestSort{
   public static void main(String[] args) {
        try{
            //sort para un sistema de fracciones
        System.out.println("Test 1");
        SistemaFraccion f = new SistemaFraccion(8);
        f.set(7, 2, 3);
        f.set(4, 2, 4);
        f.set(5, 2, 5);
        f.set(2, 2, 6);
        f.set(1, 2, 7);
        f.set(3, 2, 8);
        f.set(6, 2, 9);
        f.set(0, 2, 10);
        System.out.println("Vector sin ordenar");
        System.out.println(f);
        f.sort(f.getFracciones());
        System.out.println("Vector ordenado");
        System.out.println(f);
            //sort para un arreglo de enteros
        System.out.println("Test 2");
        Integer numeros []={4,8,9,4,6,2,3,4,7,2,1,5}; 
        System.out.println("Vector sin ordenar");
        System.out.println(Arrays.toString(numeros));
        f.sort(numeros);   
        System.out.println("Vector ordenado");
        System.out.println(Arrays.toString(numeros));
            //sort para un sistema de fracciones random
        System.out.println("Test 3");  
        Scanner sc = new Scanner(System.in);
        System.out.println("ingrese el tamaño del vector:");
            //leemos el tamaño del vector y lo creamos
        SistemaFraccion f2 = new SistemaFraccion(sc.nextInt());
        f2.crearAleatorioSinRepetir((f2.getFracciones().length+1),(f2.getFracciones().length+1));
            //generamos los datos para el vector y los imprimimos
        System.out.println("Vector sin ordenar");
        System.out.println(f2);
            //lo ordenamos y lo imprimimos
        System.out.println("Vector ordenado");    
        f2.sort(f2.getFracciones());
        System.out.println(f2);
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }  
}
